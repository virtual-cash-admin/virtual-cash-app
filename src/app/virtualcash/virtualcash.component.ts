import { VirtualCashSpeechService } from './../virtual-cash-speech.service';
import { Customer } from './../customer';
import { VirtualCashRestService } from './../virtual-cash-rest.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-virtualcash',
  templateUrl: './virtualcash.component.html',
  styleUrls: ['./virtualcash.component.css']
})
export class VirtualcashComponent implements OnInit, OnDestroy {




  customerName: string;
  userId: string;

  clicks: string[];
  clickSub: Subscription;

  errorSub: Subscription;
  errorMsg: string;

  constructor(public rest: VirtualCashRestService,
              private router: Router,
              private route: ActivatedRoute,
              public speech: VirtualCashSpeechService) {
  }

  ngOnInit(): void {
    this.speech.init();
    this._listenClicks();
    this._listenErrors();
    const userId: string = this.route.snapshot.params.userId;
    this.userId = userId;
    this.getCustomerName(userId);
  }

  ngOnDestroy(): void {
    this.clickSub.unsubscribe();
    this.errorSub.unsubscribe();
  }

  private _listenClicks(): void {
    this.clickSub = this.speech.words$
      .subscribe(
        click => {
          this._setError();
          console.log('click:', click);
          if (click.word === 'proceed'){
            this.createPot();
          }
        }
      );
  }

  private _listenErrors(): void {
    this.errorSub = this.speech.errors$
      .subscribe(err => this._setError(err));
  }

  getCustomerName(userId: string): void {
    this.rest.getCustomer(userId).subscribe((resp: any) => {
      const customer: Customer = resp;
      this.customerName = customer.name;
    });
  }

  createPot(): void {
    this.router.navigate(['/virtualcash-add/' + this.userId]);
  }


  private _setError(err?: any): void {
    if (err) {
      console.log('Speech Recognition:', err);
      this.errorMsg = err.message;
    } else {
      this.errorMsg = null;
    }
  }

}
