import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VirtualcashComponent } from './virtualcash.component';

describe('VirtualcashComponent', () => {
  let component: VirtualcashComponent;
  let fixture: ComponentFixture<VirtualcashComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VirtualcashComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VirtualcashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
