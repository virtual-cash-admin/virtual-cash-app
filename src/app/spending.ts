export interface Spending {
  desc: string;
  amountSpent: string;
  tnxDate: string;
}
