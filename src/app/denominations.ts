export interface Denominations {
  noOf50P: string;
  tv50P: string;
  noOf20P: string;
  tv20P: string;
  noOf10P: string;
  tv10P: string;
  noOf5P: string;
  tv5P: string;
  noOf1P: string;
  tv1P: string;
}


