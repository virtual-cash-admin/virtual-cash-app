import { TestBed } from '@angular/core/testing';

import { VirtualCashRestService } from './virtual-cash-rest.service';

describe('VirtualCashRestService', () => {
  let service: VirtualCashRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VirtualCashRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
