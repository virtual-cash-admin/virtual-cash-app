import { Subscription } from 'rxjs';
import { VirtualCashSpeechService } from './../virtual-cash-speech.service';

import { Router, ActivatedRoute } from '@angular/router';
import { VirtualCashRestService } from './../virtual-cash-rest.service';
import { Customer } from './../customer';
import { Component, OnInit, OnDestroy } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';



@Component({
  selector: 'app-virtualcashdetail',
  templateUrl: './virtualcashdetail.component.html',
  styleUrls: ['./virtualcashdetail.component.css']
})
export class VirtualcashdetailComponent  implements OnInit , OnDestroy {


  customer: Customer;
  graphicalRep: boolean;
  clicks: string[];
  clickSub: Subscription;
  errorSub: Subscription;
  errorMsg: string;

  constructor(public rest: VirtualCashRestService,
              private router: Router,
              private route: ActivatedRoute,
              public speech: VirtualCashSpeechService) {

               }

  ngOnInit(): void {
    this.speech.init();
    this._listenClicks();
    this._listenErrors();
    const userId: string = this.route.snapshot.params.userId;
    this.getCustomer(userId);
  }

  ngOnDestroy(): void {
    this.clickSub.unsubscribe();
    this.errorSub.unsubscribe();
  }


  private _listenClicks(): void {
    this.clickSub = this.speech.words$
      .subscribe(
        click => {
          this._setError();
          console.log('click:', click);
          if (click.word === 'back'){
            this.edit();
          }
          if (click.word === 'graphics'){
            this.graphicalRep = true;
          }
          if (click.word === 'text'){
            this.graphicalRep = false;
          }
          if (click.word === 'delete'){
            this.deactivate();
          }
          if (click.word === 'info'){
            this.showInformation();
          }
        }
      );
  }

  private _listenErrors(): void {
    this.errorSub = this.speech.errors$
      .subscribe(err => this._setError(err));
  }

  private _setError(err?: any): void {
    if (err) {
      console.log('Speech Recognition:', err);
      this.errorMsg = err.message;
    } else {
      this.errorMsg = null;
    }
  }


  getCustomer(userId: string): void {
    this.rest.getCustomer(userId).subscribe((resp: any) => {
      this.customer = resp;
    });
  }

  deactivate(): void {
    this.rest.deactivatePot(this.customer.userId).subscribe((result) => {
      this.router.navigate(['/virtualcash-add/' + this.customer.userId]);
    }, (err) => {
      console.log(err);
    });
  }

  edit(): void {
    this.router.navigate(['/virtualcash-add/' + this.customer.userId]);
  }

  showInformation(): void {
    let msg = 'In absence of actual transaction system available the Spend functionality ' +
      'is provided. execute following API to spend: <br><br>' +
      'http://localhost:8080/api/virtual-cash/spend/jaggisb/PhoneBill/341/10-07-2020 <br><br>' +
      'Instead of jaggisb use user id you are using to run MVP.<br>' +
      'Instead of PhoneBull use any description for spending.<br>' +
      'Instead of 341 use any amount you would like to spend.<br>' +
      'instead of 10-07-2020 (mm-dd-yyyy) use any date when you would have spent.<br>';

    Swal.fire({
      html: '<html>' +
        '<body>' +
        '<h6 style="font-family:Verdana, Geneva, Tahoma, sans-serif;background-color:#white;color: #1d7b8a;;font-weight: bolder;">' + msg + '</h6>' +
        '</body>' +
        '</html>',
      hideClass: {
        popup: 'swal2-hide',
        backdrop: 'swal2-backdrop-hide',
        icon: 'swal2-icon-hide'
      },
      showConfirmButton: false,
      width: 850,
      position: 'top-start',
    });
  }


  onCheckboxChange(e): void {
    if (e.target.checked) {
      this.graphicalRep = true;
    } else {
      this.graphicalRep = false;
    }

  }

  public pieSliceClickEvent(e: any): void {
  }

}
