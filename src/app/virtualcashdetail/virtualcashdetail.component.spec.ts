import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VirtualcashdetailComponent } from './virtualcashdetail.component';

describe('VirtualcashdetailComponent', () => {
  let component: VirtualcashdetailComponent;
  let fixture: ComponentFixture<VirtualcashdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VirtualcashdetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VirtualcashdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
