import { TestBed } from '@angular/core/testing';

import { VirtualCashSpeechService } from './virtual-cash-speech.service';

describe('VirtualCashSpeechService', () => {
  let service: VirtualCashSpeechService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VirtualCashSpeechService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
