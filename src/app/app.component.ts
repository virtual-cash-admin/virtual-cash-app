import { VirtualCashSpeechService } from './virtual-cash-speech.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'virtual-cash-app';

  constructor(public speech: VirtualCashSpeechService) {}
}
