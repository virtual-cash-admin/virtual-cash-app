import { VirtualCashSpeechService } from './virtual-cash-speech.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VirtualcashComponent } from './virtualcash/virtualcash.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VirtualcashaddComponent } from './virtualcashadd/virtualcashadd.component';
import { VirtualcashdetailComponent } from './virtualcashdetail/virtualcashdetail.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatNativeDateModule} from '@angular/material/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatRadioModule} from '@angular/material/radio';
import {MatSliderModule} from '@angular/material/slider';
import { ChartsModule } from 'ng2-charts';
import { IgxPieChartModule } from 'igniteui-angular-charts';

@NgModule({
  declarations: [
    AppComponent,
    VirtualcashComponent,
    VirtualcashaddComponent,
    VirtualcashdetailComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatTooltipModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    MatRadioModule,
    ChartsModule,
    IgxPieChartModule,
    MatSliderModule
  ],
  providers: [VirtualCashSpeechService],
  bootstrap: [AppComponent]
})
export class AppModule { }
