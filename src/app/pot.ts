import { Piechart } from './piechart';
import { Spending } from './spending';

export interface Pot {
      potName: string;
      allocatedAmount: string;
      remainingBalance: string;
      expiryDate: string;
      active: boolean;
      accountId: string;
      spendings: Spending[];
      pieChartSpendings: Piechart [];
}
