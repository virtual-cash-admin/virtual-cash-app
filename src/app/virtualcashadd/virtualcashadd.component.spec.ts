import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VirtualcashaddComponent } from './virtualcashadd.component';

describe('VirtualcashaddComponent', () => {
  let component: VirtualcashaddComponent;
  let fixture: ComponentFixture<VirtualcashaddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VirtualcashaddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VirtualcashaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
