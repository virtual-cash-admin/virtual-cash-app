import { Subscription } from 'rxjs';
import { VirtualCashSpeechService } from './../virtual-cash-speech.service';
import { Denominations } from './../denominations';
import { Alert } from './../alert';
import { Customer } from './../customer';
import { VirtualCashRestService } from './../virtual-cash-rest.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import Swal from 'sweetalert2/dist/sweetalert2.js';



@Component({
  selector: 'app-virtualcashadd',
  templateUrl: './virtualcashadd.component.html',
  styleUrls: ['./virtualcashadd.component.css']
})
export class VirtualcashaddComponent implements OnInit, OnDestroy {

  customer: Customer;
  accountId: string;
  alerts: Alert[] = [];
  remaningAllocationDenominations: Denominations;
  budgetAllocationDenominations: Denominations;

  clicks: string[];
  clickSub: Subscription;

  names: string[];
  nameSub: Subscription;

  allocations: string[];
  allocationSub: Subscription;

  accounts: string[];
  accountSub: Subscription;

  dates: string[];
  dateSub: Subscription;

  errorSub: Subscription;
  errorMsg: string;



  constructor(public rest: VirtualCashRestService,
              private router: Router,
              private route: ActivatedRoute,
              public dialog: MatDialog,
              public speech: VirtualCashSpeechService) {

  }
  ngOnDestroy(): void {
    this.clickSub.unsubscribe();
    this.errorSub.unsubscribe();
    this.nameSub.unsubscribe();
    this.allocationSub.unsubscribe();
    this.accountSub.unsubscribe();
    this.dateSub.unsubscribe();
  }

  ngOnInit(): void {
    this.speech.init();
    this._listenClicks();
    this._listenNames();
    this._listenAllocations();
    this._listenAccounts();
    this._listenDates();
    this._listenErrors();
    const userId: string = this.route.snapshot.params.userId;
    this.getCustomer(userId);
    this.getAlerts(userId);
    this.getbudgetAllocationDenominationsDenominations(userId);
    this.getremaningAllocationDenominations(userId);
  }

  private _listenDates(): void {
    this.dateSub = this.speech.words$
      .subscribe(
        date => {
          this._setError();
          if (date.type === 'date') {
            this._setError();
            console.log('date:', date);
            this.customer.activePot.expiryDate = date.word;

          }
        }
      );
  }

  private _listenClicks(): void {
    this.clickSub = this.speech.words$
      .subscribe(
        click => {
          if (click.type === 'click') {
            this._setError();
            console.log('click:', click);
            if (click.word === 'back') {
              this.navigateToWelcome();
            }
            if (click.word === 'create' || click.word === 'update') {
              this.createOrUpdatePot();
            }
            if (click.word === 'budget') {
              this.showBADenominations('BA');
            }
            if (click.word === 'left') {
              this.showRBDenominations('RB');
            }
            if (click.word === 'summary') {
              this.navigateToSummary();
            }
            if (click.word === 'alerts') {
              this.showAlerts();
            }
          }
        }
      );
  }

  private _listenNames(): void {
    this.nameSub = this.speech.words$
      .subscribe(
        name => {
          if (name.type === 'name') {
            this._setError();
            console.log('name:', name);
            if (!this.customer.activePot.active) {
              this.customer.activePot.potName = name.word;
            }
          }
        }
      );
  }


  private _listenAccounts(): void {
    this.accountSub = this.speech.words$
      .subscribe(
        account => {
          if (account.type === 'account') {
            this._setError();
            console.log('account:', account);
            if (!this.customer.activePot.active) {
              this.accountId = account.word;
            }
          }
        }
      );
  }


  private _listenAllocations(): void {
    this.allocationSub = this.speech.words$
      .subscribe(
        allocation => {
          if (allocation.type === 'allocation') {
            this._setError();
            console.log('allocation:', allocation);
            this.customer.activePot.allocatedAmount = allocation.word;
          }
        }
      );
  }



  private _listenErrors(): void {
    this.errorSub = this.speech.errors$
      .subscribe(err => this._setError(err));
  }



  private _setError(err?: any): void {
    if (err) {
      console.log('Speech Recognition:', err);
      this.errorMsg = err.message;
    } else {
      this.errorMsg = null;
    }
  }


  getCustomer(userId: string): void {
    this.rest.getCustomer(userId).subscribe((resp: any) => {
      this.customer = resp;
      if (this.customer.activePot.active === true) {
        this.accountId = this.customer.activePot.accountId;
      }
    });
  }

  getAlerts(userId: string): void {
    this.rest.getAlerts(userId).subscribe((resp: any) => {
      this.alerts = resp;
    });
  }

  getbudgetAllocationDenominationsDenominations(userId: string): void {
    this.rest.getDenominations(userId, 'BA').subscribe((resp: any) => {
      this.budgetAllocationDenominations = resp;
    });
  }

  getremaningAllocationDenominations(userId: string): void {
    this.rest.getDenominations(userId, 'RB').subscribe((resp: any) => {
      this.remaningAllocationDenominations = resp;
    });
  }


  createOrUpdatePot(): void {
    console.log('check the account ID');
    console.log(this.accountId);
    let errorMessages = '';
    if (this.customer.activePot.potName === '') {
      errorMessages = errorMessages + 'It is mandatory to name your POT. &nbsp;&nbsp;&nbsp;&nbsp;<br>';
    }
    if (this.customer.activePot.allocatedAmount === '') {
      errorMessages = errorMessages + 'It is mandatory to allocate funds to the POT. &nbsp;&nbsp;&nbsp;&nbsp;<br>';
    }
    if (this.customer.activePot.expiryDate === null) {
      errorMessages = errorMessages + 'It is mandatory to provide expiry date for the POT. &nbsp;&nbsp;&nbsp;&nbsp;<br>';
    }
    if (this.accountId == null) {
      errorMessages = errorMessages + 'It is mandatory to select an account. &nbsp;&nbsp;&nbsp;&nbsp;<br>';
    }

    if (errorMessages.length > 0) {
      Swal.fire({
        html: '<html>' +
          '<body>' +
          '<h6 style="font-family:Verdana, Geneva, Tahoma, sans-serif;background-color:#white;color: darkred;font-weight: bolder;width: 300px;">' + errorMessages + '</h6>' +
          '</body>' +
          '</html>',
        hideClass: {
          popup: 'swal2-hide',
          backdrop: 'swal2-backdrop-hide',
          icon: 'swal2-icon-hide'
        },
        showConfirmButton: false,
        width: 400,

        position: 'top-start',
      });
      return;
    }
    console.log('i am going here 1');

    if (this.customer.activePot.active === true) {
      console.log('i am going here');
      this.accountId = this.customer.activePot.accountId;
    }
    this.rest.createOrUpdatePot(this.customer.userId,
      this.customer.activePot.potName,
      this.customer.activePot.allocatedAmount,
      this.customer.activePot.expiryDate,
      this.accountId).subscribe((result) => {
        this.router.navigate(['/virtualcash-details/' + this.customer.userId]);
      }, (err) => {
        console.log(err);
      });
  }

  navigateToSummary(): void {
    this.router.navigate(['/virtualcash-details/' + this.customer.userId]);
  }

  navigateToWelcome(): void {
    this.router.navigate(['/welcome/' + this.customer.userId]);
  }

  showAlerts(): void {
    let allAlertMessages = '';
    for (const alert of this.alerts) {
      allAlertMessages = allAlertMessages.concat(alert.alertMessage);
      allAlertMessages = allAlertMessages.concat('<br>');
    }
    Swal.fire({
      html: '<html>' +
        '<body>' +
        '<h6 style="font-family:Verdana, Geneva, Tahoma, sans-serif;background-color:#white;color: darkred;font-weight: bolder;width: 200px;">' + allAlertMessages + '</h6>' +
        '</body>' +
        '</html>',
      hideClass: {
        popup: 'swal2-hide',
        backdrop: 'swal2-backdrop-hide',
        icon: 'swal2-icon-hide'
      },
      showConfirmButton: false,
      width: 250,

      position: 'top-start',
    });
  }



  showRBDenominations(type: string): void {
    Swal.fire({
      html: '<html>' +
        '<body>' +
        '<div style="font-family:Verdana, Geneva, Tahoma, sans-serif;background-color:#white;color: #1d7b8a;font-weight: bolder;width: 300px;">' +
        '<img src="../../assets/img/50.jpg">' + '    x ' + this.remaningAllocationDenominations.noOf50P + ' = £' + this.remaningAllocationDenominations.tv50P +
        '<br>' +
        '<img src="../../assets/img/20.jpg">' + '    x ' + this.remaningAllocationDenominations.noOf20P + ' = £' + this.remaningAllocationDenominations.tv20P +
        '<br>' +
        '<img src="../../assets/img/10.jpg">' + '    x ' + this.remaningAllocationDenominations.noOf10P + ' = £' + this.remaningAllocationDenominations.tv10P +
        '<br>' +
        '<img src="../../assets/img/5.jpg">' + '    x ' + this.remaningAllocationDenominations.noOf5P + ' = £' + this.remaningAllocationDenominations.tv5P +
        '<br>' +
        '<img src="../../assets/img/1.jpg">' + '    x ' + this.remaningAllocationDenominations.noOf1P + ' = £' + this.remaningAllocationDenominations.tv1P +
        '</body>' +
        '</div>' +
        '</html>',
      showConfirmButton: false,
      width: 350,
      timer: 5000,
      position: 'top-start',
    });
  }

  showBADenominations(type: string): void {
    Swal.fire({
      html: '<html>' +
        '<body>' +
        '<div style="font-family:Verdana, Geneva, Tahoma, sans-serif;background-color:#white;color: #1d7b8a;font-weight: bolder;width: 300px;">' +
        '<img src="../../assets/img/50.jpg">' + '    x ' + this.budgetAllocationDenominations.noOf50P + ' = £' + this.budgetAllocationDenominations.tv50P +
        '<br>' +
        '<img src="../../assets/img/20.jpg">' + '    x ' + this.budgetAllocationDenominations.noOf20P + ' = £' + this.budgetAllocationDenominations.tv20P +
        '<br>' +
        '<img src="../../assets/img/10.jpg">' + '    x ' + this.budgetAllocationDenominations.noOf10P + ' = £' + this.budgetAllocationDenominations.tv10P +
        '<br>' +
        '<img src="../../assets/img/5.jpg">' + '    x ' + this.budgetAllocationDenominations.noOf5P + ' = £' + this.budgetAllocationDenominations.tv5P +
        '<br>' +
        '<img src="../../assets/img/1.jpg">' + '    x ' + this.budgetAllocationDenominations.noOf1P + ' = £' + this.budgetAllocationDenominations.tv1P +
        '</body>' +
        '</div>' +
        '</html>',
      showConfirmButton: false,
      width: 350,
      timer: 5000,
      position: 'top-start',
    });
  }
}
