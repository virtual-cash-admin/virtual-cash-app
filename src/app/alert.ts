export interface Alert {
  fieldName: string;
  alertMessage: string;
}
