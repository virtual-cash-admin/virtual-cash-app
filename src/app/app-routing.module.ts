import { VirtualcashComponent } from './virtualcash/virtualcash.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VirtualcashaddComponent } from './virtualcashadd/virtualcashadd.component';
import { VirtualcashdetailComponent } from './virtualcashdetail/virtualcashdetail.component';

const routes: Routes = [
  {
    path: 'welcome/:userId',
    component: VirtualcashComponent,
    data: { title: 'VirtualCash List' }
  },
  {
    path: 'virtualcash-details/:userId',
    component: VirtualcashdetailComponent,
    data: { title: 'virtualcash Details' }
  },
  {
    path: 'virtualcash-add/:userId',
    component: VirtualcashaddComponent,
    data: { title: 'virtualcash Add' }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
