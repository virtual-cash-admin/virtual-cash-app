import { Injectable, NgZone } from '@angular/core';
import { Subject } from 'rxjs';

declare var annyang: any;

@Injectable({
  providedIn: 'root'
})
export class VirtualCashSpeechService {


  words$ = new Subject<{ [key: string]: string }>();
  errors$ = new Subject<{ [key: string]: any }>();
  listening = false;
  constructor(private zone: NgZone) { }

  get speechSupported(): boolean {
    return !!annyang;
  }

  init(): void {
    const commands = {
      'click :click': (click) => {
        this.zone.run(() => {
          this.words$.next({ type: 'click', word: click });
        });
      },
      'name :name': (name) => {
        this.zone.run(() => {
          this.words$.next({ type: 'name', word: name });
        });
      },
      'allocation :allocation': (allocation) => {
        this.zone.run(() => {
          this.words$.next({ type: 'allocation', word: allocation });
        });
      },
      'expires :expires': (expires) => {
        this.zone.run(() => {
          this.words$.next({ type: 'expires', word: expires });
        });
      },
      'account :account': (account) => {
        this.zone.run(() => {
          this.words$.next({ type: 'account', word: account });
        });
      }

    };
    annyang.addCommands(commands);

    annyang.addCallback('result', (userSaid) => {
      console.log('User may have said:', userSaid);
    });
    annyang.addCallback('errorNetwork', (err) => {
      this._handleError('network', 'A network error occurred.', err);
    });
    annyang.addCallback('errorPermissionBlocked', (err) => {
      this._handleError('blocked', 'Browser blocked microphone permissions.', err);
    });
    annyang.addCallback('errorPermissionDenied', (err) => {
      this._handleError('denied', 'User denied microphone permissions.', err);
    });
    annyang.addCallback('resultNoMatch', (userSaid) => {
      this._handleError(
        'no match',
        'Spoken command not recognized',
        { results: userSaid });
    });
  }

  private _handleError(error, msg, errObj): void {
    this.zone.run(() => {
      this.errors$.next({
        error: error,
        message: msg,
        obj: errObj
      });
    });
  }

  startListening(): void {
    annyang.start();
    this.listening = true;
  }

  abort(): void {
    annyang.abort();
    this.listening = false;
  }
}
