export interface Piechart {
  share: number;
  name: string;
}
