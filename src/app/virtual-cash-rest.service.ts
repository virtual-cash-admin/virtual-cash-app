import { Piechart } from './piechart';
import { Pot } from './pot';
import { Denominations } from './denominations';
import { Customer } from './customer';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/internal/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

//const endpoint = 'http://jmdvirtualcashapis-env.eba-9muejx5h.ap-south-1.elasticbeanstalk.com/api/virtual-cash/';
const endpoint = 'http://localhost:8080/api/virtual-cash/';

@Injectable({
  providedIn: 'root'
})
export class VirtualCashRestService {

  constructor(private http: HttpClient) { }



  getCustomers(): Observable<any> {
    return this.http.get<Customer>(endpoint + 'getAll').pipe(
      catchError(this.handleError)
    );
  }

  getCustomer(id: string): Observable<any> {
    return this.http.get<Customer>(endpoint + 'get/' + id).pipe(
      catchError(this.handleError)
    );
  }


  createOrUpdatePot(userId: string, potName: string, allocatedAmount: string, expiryDate: string, accountId: string): Observable<any> {
    return this.http.get(endpoint + 'createOrUpdatePot/' +
      userId + '/' + potName + '/' + allocatedAmount + '/' + expiryDate + '/' + accountId).pipe(
        catchError(this.handleError)
      );
  }

  deactivatePot(userId: string): Observable<any> {
    return this.http.get(endpoint + 'deactivePot/' + userId).pipe(
        catchError(this.handleError)
      );
  }

  getAlerts(userId: string): Observable<any> {
    return this.http.get(endpoint + 'alerts/' + userId).pipe(
      catchError(this.handleError)
    );
  }

 getDenominations(userId: string, type: string): Observable<any> {
  return this.http.get<Denominations>(endpoint + 'denominations/' + userId + '/' + type).pipe(
    catchError(this.handleError)
  );
 }


  private handleError(error: HttpErrorResponse): any {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }


}
