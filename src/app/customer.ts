import { Pot } from './pot';
import { Account } from './account';
export interface Customer {
  userId: string;
  name: string;
  accounts: Account[];
  usedPots: Pot[];
  activePot: Pot;
}
